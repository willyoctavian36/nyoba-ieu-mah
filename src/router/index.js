import { createRouter, createWebHistory } from 'vue-router';
import BookDetails from './../pages/BookDetails.vue';
import HomeBooks from './../pages/HomeBooks.vue'
import Nyoba from '@/pages/Nyoba.vue'
import PhoneInput from '@/pages/Phone.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeBooks
  },
  {
    path: '/book/:id',
    name: 'BookDetails',
    component: BookDetails,
    props: true
  },
  {
    path: '/nyobain',
    name: 'Nyoba',
    component: Nyoba,
  },
  {
    path: '/phone-input',
    name: 'PhoneComponent',
    component: PhoneInput
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;